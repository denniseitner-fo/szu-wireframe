$(document).ready(function() {


    function initMap() {

        var myLatLng = {
            lat: 47.362130,
            lng: 8.521553
        };

        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 14,
            center: myLatLng
        });

        var styles =

            [{
                "elementType": "geometry",
                "stylers": [{
                    "color": "#242f3e"
                }]
            }, {
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#746855"
                }]
            }, {
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#242f3e"
                }]
            }, {
                "featureType": "administrative.locality",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#d59563"
                }]
            }, {
                "featureType": "poi",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#d59563"
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#263c3f"
                }]
            }, {
                "featureType": "poi.park",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#6b9a76"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#38414e"
                }]
            }, {
                "featureType": "road",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#212a37"
                }]
            }, {
                "featureType": "road",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#9ca5b3"
                }]
            }, {
                "featureType": "road.arterial",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#746855"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#1f2835"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "labels",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "road.highway",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#f3d19c"
                }]
            }, {
                "featureType": "road.local",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "transit",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#2f3948"
                }]
            }, {
                "featureType": "transit.line",
                "stylers": [{
                    "color": "#ff000f"
                }, {
                    "visibility": "on"
                }, {
                    "weight": 8
                }]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry.fill",
                "stylers": [{
                    "color": "#ff000f"
                }, {
                    "visibility": "on"
                }, {
                    "weight": 3
                }]
            }, {
                "featureType": "transit.line",
                "elementType": "geometry.stroke",
                "stylers": [{
                    "color": "#ff000f"
                }, {
                    "visibility": "on"
                }, {
                    "weight": 2
                }]
            }, {
                "featureType": "transit.station",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#d59563"
                }]
            }, {
                "featureType": "transit.station.rail",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#ff000f"
                }, {
                    "visibility": "on"
                }, {
                    "weight": 5.5
                }]
            }, {
                "featureType": "transit.station.rail",
                "elementType": "geometry.fill",
                "stylers": [{
                    "visibility": "off"
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry",
                "stylers": [{
                    "color": "#17263c"
                }]
            }, {
                "featureType": "water",
                "elementType": "geometry.fill",
                "stylers": [{
                    "saturation": -65
                }, {
                    "visibility": "on"
                }, {
                    "weight": 4
                }]
            }, {
                "featureType": "water",
                "elementType": "labels.text.fill",
                "stylers": [{
                    "color": "#515c6d"
                }]
            }, {
                "featureType": "water",
                "elementType": "labels.text.stroke",
                "stylers": [{
                    "color": "#17263c"
                }]
            }];

        map.setOptions({
            styles: styles

        });

        var marker = new google.maps.Marker({
            position: myLatLng,
            map: map,

        });
    }
});
