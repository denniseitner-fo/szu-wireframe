$(document).ready(function() {

    if ($('.navbar-toggle').is(":hidden")) {

        //    show all submenu at once
        $('.dropdown').hover(function() {
            $(".dropdown").toggleClass('open');
        });


        //match height submenu
        function equal_cols(el) {
            var h = 0;
            $(el).each(function() {
                $(this).css({
                    'height': 'auto'
                });
                if ($(this).outerHeight() > h) {
                    h = $(this).outerHeight();
                }
            });
            $(el).each(function() {
                $(this).css({
                    'height': h
                });
            });
        }


        // match height for boxes
        $('.item').matchHeight();
        // match height for boxes
        $('.eqh').matchHeight();

        $('#pickdate').datetimepicker({
            format: 'DD.MM.YYYY',
            minDate: new Date(),
        });

        $('#picktime').datetimepicker({
            format: 'HH:mm',

        });

        //only in expaneded version
        equal_cols('.dropdown-menu');

        //fix carousel padding
        $('.carousel').parent('.content').css('margin-right', 0);

    }; //endif


    //Call to modal directly with # symbol in url
    if (window.location.hash) {
        var hash = window.location.hash;
        $(hash).modal('toggle');
    }

    //open the modal on .box-roll up link
    $(".box-rollup").on("click", function() {
        $('#myModal').modal('toggle');
    });


    //Featherlight Gallery
    $('.gallery').featherlightGallery({
        gallery: {
            fadeIn: 300,
            fadeOut: 300
        },
        openSpeed: 300,
        closeSpeed: 300,
        closeIcon: '<i class="fa fa-fw" aria-hidden="true" title="Copy to use close"></i>',
        previousIcon: '',
        /* Code that is used as previous icon */
        nextIcon: '',
        /* Code that is used as next icon */
    });


});
