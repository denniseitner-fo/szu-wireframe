var gulp            = require('gulp');
var sass            = require('gulp-sass');
var watch           = require('gulp-watch');
var browserSync     = require('browser-sync');
var reload          = browserSync.reload;
var concat          = require('gulp-concat');
var jshint          = require('gulp-jshint');
var del             = require('del');
var uglify          = require('gulp-uglify');
var imagemin        = require('gulp-imagemin');
var sourcemaps      = require('gulp-sourcemaps');
var autoprefixer    = require('gulp-autoprefixer');
var rubysass        = require('gulp-ruby-sass');
var htmlreplace     = require('gulp-html-replace');
var htmlbeautify    = require('gulp-html-beautify');
var gutil           = require('gulp-util');
var ftp             = require('vinyl-ftp');
var gcmq            = require('gulp-group-css-media-queries');
var uncss           = require('gulp-uncss');
var uglifycss       = require('gulp-uglifycss');
var cssbeautify     = require('gulp-cssbeautify');



// source and distribution folder
var source = 'src/';
var dest = 'dist/assets/';

// Bootstrap scss source
var bootstrapSass = {
        in: './node_modules/bootstrap-sass/'
    };

// fonts
var fonts = {
        in: [source + 'fonts/*.*', bootstrapSass.in + 'assets/fonts/**/*'],
        out: dest + 'fonts/'
    };

// css source file: .scss files
var scss = {
    in: source + 'scss/main.scss',
    out: dest + 'css/',
    watch: source + 'scss/**/*',

    sassOpts: {
        outputStyle: 'nested',
        precison: 3,
        errLogToConsole: true,
        includePaths: [bootstrapSass.in + 'assets/stylesheets']
    }
};

// copy bootstrap required fonts to dest
gulp.task('fonts', function () {
    return gulp
        .src(fonts.in)
        .pipe(gulp.dest(fonts.out));
});

// compile scss
gulp.task('sass', ['fonts'], function () {
    return gulp.src(scss.in)
        .pipe(autoprefixer())
        .pipe(sourcemaps.init())
        .pipe(sass().on('error', sass.logError))
        .pipe(sass(scss.sassOpts))
        .pipe(gulp.dest(scss.out))
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('src/assets/css'))
});


gulp.task('browser-sync', function() {
    browserSync({
      port: 3200,

      ui: {
        port: 3200
      },
      server: {
          baseDir: "src" // Change this to your web root dir
      },
        files: ["src/assets/css/*.css", "src/assets/js/*.js", "src/*.html"],
    });
});


gulp.task('css', function () {
  gulp.src(['src/assets/css/*.css'])
    .pipe(concat('main.css'))
    .pipe(gcmq())
    .pipe(uglifycss({
      "maxLineLen": 80,
      "uglyComments": true
    }))
    .pipe(gulp.dest('dist/assets/css'))
});

gulp.task('js', function () {
   return gulp.src(['src/assets/js/jquery.min.js', 'src/assets/js/bootstrap.min.js', 'src/assets/js/moment.js', 'src/assets/js/bootstrap-datetimepicker.min.js', 'src/assets/js/bootstrap.offcanvas.min.js', 'src/assets/js/jquery.matchheight.js', 'src/assets/js/app.js', 'src/assets/js/map.js',])
      .pipe(jshint())
      .pipe(jshint.reporter('default'))
      //.pipe(uglify())
      .pipe(concat('app.js'))
      .pipe(gulp.dest('dist/assets/js'))
});

gulp.task('clean', function () {
  return del([
    'dist/*',
  ]);
});

gulp.task('copy-images', function () {
  gulp.src('src/assets/images/*')
      .pipe(imagemin())
      .pipe(gulp.dest('src/assets/images'))
      .pipe(gulp.dest('dist/assets/images'))
});

gulp.task('copy-fonts', function () {
  gulp.src('src/assets/fonts/**/*')
      .pipe(gulp.dest('dist/assets/fonts'))
});


gulp.task('build-html', function() {
  gulp.src('src/*.html')
    .pipe(htmlreplace({
        'js': 'assets/js/app.js'
    }))
    .pipe(gulp.dest('dist/'));
});

gulp.task('htmlbeautify', function() {
  var options = {
    "indent_size": 4
  };
  gulp.src('src/*.html')
    .pipe(htmlbeautify(options))
    .pipe(gulp.dest('src/'))
});

gulp.task('cssbeautify', function() {
    return gulp.src('src/scss/theme/*.scss')
        .pipe(cssbeautify({
            indent: '  ',
            openbrace: 'separate-line',
            autosemicolon: true
        }))
        .pipe(gulp.dest('src/scss/'));;
});


gulp.task( 'deploy', function () {
    var conn = ftp.create( {
        host:     'cyberfactory.ch',
        user:     'cf-transfer',
        password: 'z5YpT35KL',
        parallel: 10,
        log:      gutil.log
    } );

    var globs = [
        'dist/*',
        'dist/assets/**',
    ];

    // using base = '.' will transfer everything to /public_html correctly
    // turn off buffering in gulp.src for best performance

    return gulp.src( globs, { base: '.', buffer: false } )
        .pipe( conn.newer( '/szu/v03' ) ) // only upload newer files
        .pipe( conn.dest( '/szu/v03' ) );
} );

gulp.task('uncss', function () {
    return gulp.src('dist/assets/css/main.css')
        .pipe(uncss({
          //  html: ['index.html', 'posts/**/*.html', 'http://example.com']
          html: ['dist/*.html']
        }))
        .pipe(gulp.dest('./out'));
});


// default task
gulp.task('default', ['sass', 'htmlbeautify', 'browser-sync'], function () {
     gulp.watch(scss.watch, ['sass']);
});

gulp.task('build', ['clean', 'build-html', 'copy-images', 'copy-fonts', 'css', 'js']);
