<script>
    // Screen size ID will be stored in this variable (global var for JS)
    var CurrentBootstrapScreenSize = 'unknown';

    $(document).ready(function() {

        // <div> objects for all screen sizes required for screen size detection.
        // These <div> is hidden for users eyes.
        var currentScreenSizeDetectorObjects = $('<div class="label label-warning">').css({
            'position': 'absolute',
            'top': '0px',
            'width': '300px',
            'color': 'white'
        }).addClass('current-screen-size').append([
            $('<div>').addClass('device-xs visible-xs').html('&nbsp;'),
            $('<div>').addClass('device-sm visible-sm').html('&nbsp;'),
            $('<div>').addClass('device-md visible-md').html('&nbsp;'),
            $('<div>').addClass('device-lg visible-lg').html('&nbsp;')
        ]);

        // Attach <div> objects to <body>
        $('body').prepend(currentScreenSizeDetectorObjects);

        // Core method for detector
        function currentScreenSizeDetectorMethod() {
            $(currentScreenSizeDetectorObjects).find('div').each(function() {
                var className = $(this).attr('class');
                if ($(this).is(':visible')) {
                    if (String(className).match(/device-xs/)) CurrentBootstrapScreenSize = 'xs';
                    else if (String(className).match(/device-sm/)) CurrentBootstrapScreenSize = 'sm';
                    else if (String(className).match(/device-md/)) CurrentBootstrapScreenSize = 'md';
                    else if (String(className).match(/device-lg/)) CurrentBootstrapScreenSize = 'lg';
                    else CurrentBootstrapScreenSize = 'unknown';
                };
            })
            console.log('Current Bootstrap screen size is: ' + CurrentBootstrapScreenSize);
            $('.current-screen-size').first().html('Bootstrap current screen size: <b>' + CurrentBootstrapScreenSize + '</b>');
        }

        // Bind screen size and orientation change
        $(window).bind("resize orientationchange", function() {
            // Execute screen detection
            currentScreenSizeDetectorMethod();
        });

        // Execute screen detection on page initialize
        currentScreenSizeDetectorMethod();

    });
</script>


<script>
    $(document).ready(function() {
        document.addEventListener("click", function() {

            console.log('test');

        });

    });



    var resizeTimer;

    $(window).on('resize', function(e) {

        clearTimeout(resizeTimer);
        resizeTimer = setTimeout(function() {
        //    $("body").prepend("<div>" + $(window).width() + "</div>");

            window.location.reload();

        }, 250);

    });
</script>
